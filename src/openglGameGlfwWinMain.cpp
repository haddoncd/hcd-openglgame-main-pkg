#ifdef openglGameGlfwWinMain_cpp
#error Multiple inclusion
#endif
#define openglGameGlfwWinMain_cpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef print_hpp
#error "Please include print.hpp before this file"
#endif

#ifndef openglGame_common_hpp
#error "Please include openglGame_common.hpp before this file"
#endif

#ifndef Assets_cpp
#error "Please include Assets.cpp before this file"
#endif

#ifndef Gl3wLibrary_hpp
#error "Please include Gl3wLibrary.hpp before this file"
#endif

#ifndef _glfw3_h_
#error "Please include GLFW/glfw3.h before this file"
#endif

#ifndef glfw_callbackAdaptors_cpp
#error "Please include glfw_callbackAdaptors.cpp before this file"
#endif

#ifndef openglGame_updateAsset_cpp
#error "Please include openglGame_updateAsset.cpp before this file"
#endif

#include <cstdio>


void APIENTRY glDebugMessageCallback(GLenum       source,
                                     GLenum       type,
                                     GLuint       id,
                                     GLenum       severity,
                                     GLsizei      length,
                                     const GLchar *message,
                                     const void   *userParam)
{
  print(stderr, "%u, %u, %u, %u, %s\n", source, type, id, severity, message);
};

int main(u32 argc, char **argv)
{
  bool normal_exit = false;

  BREAKABLE_START
  {
    f64  scale_fudge       = 1.0;
    bool scale_fudge_set   = false;

    args_List *args;
    args_List_localAllocAndInit(args, argc, argv);
    args_ListIndex args_index = {0};

    // Argument 0 is always the executable path
    if (!args_indexValid(args, args_index))
    {
      Str_print(stdout, "Argument 0 (executable path) missing\n"_str);
      break;
    }
    Str executable_path = args_getArg(args, args_index);
    args_step(args, &args_index);

    {

      while (args_indexValid(args, args_index))
      {
        if (args_matchOpt(args, args_index, "scale-fudge"_str))
        {
          if (scale_fudge_set)
          {
            Str_print(stdout, "Warning: Option \"scale-fudge\" re-specified\n"_str);
          }

          Str param_str = args_getParam(args, &args_index);

          if (!param_str.length)
          {
            Str_print(stdout, "Warning: Option \"scale-fudge\" requires a parameter\n"_str);
          }
          else
          {
            f64 param = Str_parseF64(param_str);

            if (isnan(param) || param <= 0.0)
            {
              Str_print(stdout,
              {
                "Warning: Invalid \"scale-fudge\" parameter: \""_str,
                param_str, "\"\n"_str
              });
            }
            else
            {
              scale_fudge     = param;
              scale_fudge_set = true;
            }
          }
        }
        else
        {
          // Any other arguments must be for the game
          break;
        }

        args_step(args, &args_index);
      }
    }

    if (!glfwInit())
    {
      Str_print(stdout, "Error: glfwInit() failed\n"_str);
      break;
    }

    fprintf(stdout, "GLFW %s\n", glfwGetVersionString());

    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
    GLFWwindow *window = glfwCreateWindow(640, 480, "OpenGL Game App", NULL, NULL);
    if (!window)
    {
      Str_print(stdout, "Error: glfwCreateWindow() failed\n"_str);
      break;
    }

    glfw_setCallbacks(window);

    glfwMakeContextCurrent(window);

    glfwSwapInterval(1);

    openglGame_GameResources resources = {0};

    {
      LPVOID baseAddress = (LPVOID) (2 * SI_TEBI);

      resources.memorySize = 64 * SI_MEBI;
      resources.memory      = (u8 *)VirtualAlloc(baseAddress,
                                                 resources.memorySize,
                                                 MEM_RESERVE|MEM_COMMIT,
                                                 PAGE_READWRITE);
      // NB. VirtualAlloc initialises allocated memory to zero.
    }

    Gl3wLibrary gl3w_library = {0};
    Gl3wLibrary_init(&gl3w_library);

    if (!Gl3wLibrary_getFunctions(&gl3w_library, &resources.gl))
    {
      Str_print(stdout, "Failed to initiailze gl3w\n"_str);
      break;
    }

    resources.gl.DebugMessageCallback(&glDebugMessageCallback, 0);

    resources.assets = variableArray(void *, arrayCount(asset_list));
    resources.assetStatuses = variableArray(openglGame_AssetStatus::Enum,
                                            arrayCount(asset_list));
    u64 *asset_file_times = variableArray(u64, arrayCount(asset_list));

    for (u64 i = 0; i < arrayCount(asset_list); ++i)
    {
      resources.assets[i] = 0;
      resources.assetStatuses[i] = openglGame_AssetStatus::UNLOADED;
      asset_file_times[i] = 0;
    }

    // FIXME: ifdef debug
    resources.debugResources.platformAllocator.malloc = malloc;
    resources.debugResources.platformAllocator.free   = free;
    resources.debugResources.clipboard.copy  = glfw_copyCallback;
    resources.debugResources.clipboard.paste = glfw_pasteCallback;

    fprintf(stdout, "OpenGL %s, GLSL %s\n",
           resources.gl.GetString(GL_VERSION),
           resources.gl.GetString(GL_SHADING_LANGUAGE_VERSION));

    if (!openglGame_LIB_INIT(&resources))
    {
      Str_print(stdout, "Game library failed to initialise\n"_str);
      break;
    }

    if (!openglGame_GAME_INIT(&resources, args, args_index))
    {
      Str_print(stdout, "Game failed to initialise\n"_str);
      break;
    }

    normal_exit = true;

    while (!glfwWindowShouldClose(window))
    {
      for (u64 i = 0; i < arrayCount(asset_list); ++i)
      {
        openglGame_updateAsset(&asset_list[i],
                               &resources.assets[i],
                               &resources.assetStatuses[i],
                               &asset_file_times[i]);
      }

      openglGame_Input input = {0};

      glfw_getInput(&input);

      if (scale_fudge != 1.0)
      {
        f64 scale_multiplier = 1.0 / scale_fudge;
        input.windowWidth_points      *= scale_multiplier;
        input.windowHeight_points     *= scale_multiplier;
        input.mouse.cursorPosX_points *= scale_multiplier;
        input.mouse.cursorPosY_points *= scale_multiplier;
      }

      fflush(0);

      openglGame_GAME_UPDATE(&resources, &input);

      glfwSwapBuffers(window);

      glfwPollEvents();

      fflush(0);
    }

    openglGame_LIB_SHUTDOWN(&resources);
  }
  BREAKABLE_END

  if (!normal_exit)
  {
    Str_print(stdout, "\nPress enter to exit...\n"_str);
    fflush(0);
    getc(stdin);
  }

  return normal_exit ? 0 : 1;
}
